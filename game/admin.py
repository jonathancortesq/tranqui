# coding: utf-8
from django.contrib import admin
from .models import Player, Room, Match

admin.site.register(Player)
admin.site.register(Room)
admin.site.register(Match)


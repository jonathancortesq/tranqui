# coding: utf-8
from django import forms
from .models import Player

class PlayerForm(forms.ModelForm):
    class Meta:
        fields = ('name',)
        model = Player
        widgets = {
            'name': forms.TextInput(attrs={"class": 'form-control'}),
        }

    def save(self):
        name = self.cleaned_data['name']
        player, create = Player.objects.get_or_create(
            name=name)
        return player

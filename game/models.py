# coding: utf-8
import jsonfield
from django.db import models


class Player(models.Model):
    name = models.CharField(max_length=50)
    score = jsonfield.JSONField()

    class Meta:
        verbose_name = "Player"
        verbose_name_plural = "Players"

    def __str__(self):
        return self.name
    


class Room(models.Model):
    name = models.CharField(max_length=100)
    players = models.ManyToManyField(Player)
    rules = jsonfield.JSONField()
    score = jsonfield.JSONField()

    class Meta:
        verbose_name = "Room"
        verbose_name_plural = "Rooms"

    def __str__(self):
        return self.name


class Match(models.Model):
    room = models.ForeignKey(Room)
    score = jsonfield.JSONField()
    players_ready = jsonfield.JSONField()
    choices = jsonfield.JSONField()

    class Meta:
        verbose_name = "Match"
        verbose_name_plural = "Matchs"

    def __str__(self):
        return str(self.id)
    
    

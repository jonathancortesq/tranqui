import Axios from 'axios'
import * as Cookies from 'js-cookie'
import store from '../../store/index.js';

export default class playGround{
	constructor(props){
		this.container = props
		this.options = {"rock": 1, "spock": 2, "paper": 3, "lizard": 4, "scissors": 5}
		this.HTTP = Axios.create({
		  baseURL: '/api/',
		  headers:{
		  	'X-CSRFToken':Cookies.get('csrftoken')
		  }
		})
		this.player = store.state.Player
		this.room = store.state.roomSelected
		this.match = null
	}

	setHeader(data){
		let html = `<section id="roomHeader">
						<h3>
							${data.fields.name}
							<small class="text-muted">${data.fields.score ? 'Playing' : ''}</small>
						</h3>
						<div id="score"></div>
					</section>
					`
		this.container.innerHTML += html
		this._setRoomScore(data.fields.score)
	}

	setBody(){
		let html = `<div class="container-fluid">
						<div class="d-flex justify-content-center mb-5 mt-5">
							<div class="p-2 align-self-center" id="bodyGame">
							</div>
						</div>
					</div>
					`
		this.container.innerHTML += html
		this._enableMatch()
	}

	getPlayers(players){
		let idPlayers = players.join(',')
		Axios.get(`/api/players/?filters=${idPlayers}`).then(response =>{
			this.setPlayers(response.data.data)
		}).catch(error=>{
			console.log(error)
		})
	}

	setPlayers(players){
		const roomHeader = document.getElementById('roomHeader')
		const userListContainer = document.createElement('ul')
		userListContainer.classList.add('list-group', 'list-group-horizontal')
		players.forEach(player =>{
			let li = document.createElement('li')
			li.classList.add('list-group-item')
			li.innerHTML = player.fields.name
			userListContainer.appendChild(li)
		})
		roomHeader.appendChild(userListContainer)
	}

	groundConstructor(){
		const ground = document.createElement('section')
		let buttons = document.createElement('div')
		buttons.setAttribute('id', 'buttonsContainer')
		buttons.appendAfter(document.getElementById('roomHeader'))
		Object.keys(this.options).forEach(button=>{
			buttons.insertAdjacentHTML('beforeend', this._button(button))
			document.getElementById(button).addEventListener('click', ()=>{this._choiceSelected(button)}, false)
		})
	}

	_button(button){
		return `<button class="btn" id="${button}"><img src="/static/images/${button}.svg" style="max-width:40px; max-height:40px""/></button>`
	}

	_choiceSelected(choiceName){
		let match = this.match
		let choiceNumer = this.options[choiceName]
		let data = {
			players_ready: {},
			choices: {}
		}
		data['players_ready'][this.player.id] = true
		data['choices'][this.player.id] = choiceNumer
		this.HTTP.patch(`match/${match.id}/`, data).then(response=>{
			console.log(response.data.data.result, 'api response')
			this.setResults(response.data.data.result, choiceName)
			this._setRoomScore(response.data.data.room_score)
		}).catch(error=>{
			console.log(error)
		})
	}

	setResults(results, userChoise){
		console.log(results, 'function')
		console.log(results.winner, userChoise, (results.winner == userChoise))
		let classStyle, message;
		if(results.winner == userChoise){
			classStyle = 'success'
			message = 'You win!'
		}else if(results.winner == 'Was a tie'){
			classStyle = 'warning'
			message = results.winner
		}else{
			classStyle = 'danger'
			message = 'You lose!'
		}

		let showResults = `<div class="alert alert-${classStyle}" role="alert" id="alert">
							  ${message}
							</div>`
		let buttonsContainer = document.getElementById('buttonsContainer')
		buttonsContainer.parentNode.removeChild(buttonsContainer);
		let alertContainer = document.createElement('div')
		alertContainer.appendAfter(document.getElementById('roomHeader'))
		alertContainer.insertAdjacentHTML('beforeend', showResults)
		this._enableMatch()
	}

	_setRoomScore(data){
		let scoreContent = document.getElementById('score')
		while (scoreContent.firstChild) scoreContent.removeChild(scoreContent.firstChild);
		let badgets = ''
		Object.keys(data).forEach(key => {
			let badget = `<button type="button" class="btn">
						  ${key} <span class="badge badge-light">${data[key]}</span>
						</button>`
			badgets += badget
		});
		scoreContent.innerHTML = badgets
	}	

	_enableMatch(){
		let button = `<button type="button" class="btn btn-primary btn-lg btn-block" id="matchReadyPlayer">Match</button>`
		document.getElementById('bodyGame').insertAdjacentHTML('beforeend', button)
		document.getElementById('matchReadyPlayer').addEventListener('click', ()=>{this._waitMatch()}, false)
	}

	_waitMatch(){
		let delbutton = document.getElementById('matchReadyPlayer')
		let alert = document.getElementById('alert')
		if(alert)
			alert.parentNode.removeChild(alert);

		this.HTTP.post('match/', {room_id: this.room.pk}).then(response=>{
			this.match = response.data.data
			delbutton.parentNode.removeChild(delbutton);
			this.groundConstructor()
		}).catch(error =>{
			if(error.response.status == 400){
				let html = `<div class="alert alert-danger" role="alert" id="alert">
							  ${error.response.data.reason}
							</div>`
				delbutton.parentNode.removeChild(delbutton);
				let alertContainer = document.createElement('div')
				alertContainer.appendAfter(document.getElementById('roomHeader'))
				alertContainer.insertAdjacentHTML('beforeend', html)

			}
						
		})
	}

	render(data){
		while (this.container.firstChild) this.container.removeChild(this.container.firstChild);
		this.setHeader(data)
		this.setBody()
		this.getPlayers(data.fields.players)
	}
}
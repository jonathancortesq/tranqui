require('es6-promise').polyfill();
import "babel-polyfill";
import Axios from 'axios';
import store from '../../store/index.js';
import ListItem from './roomListItem.js'


export default class roomList {
	constructor(props){
		this.container = document.getElementById('listGroup')
		this.rooms = []
		this.container.innerHTML = `<div class="spinner-border" role="status">
									  <span class="sr-only">Loading...</span>
									</div> `
	}

	getRooms(){
		Axios.get('/api/rooms/').then(response => {
			this.rooms = response.data.data 
			this.render()
		}).catch(error=>{
			console.log(error)
		})
	}

	roomSelected(item){
		let room = this.rooms.filter(room =>{
			return room.pk == item.getAttribute('data-id')
		})[0]
		if(!store.state.roomSelected || store.state.roomSelected.pk != room.pk){
			store.dispatch('roomSelected', room )
		}
	}

	render(){
		while (this.container.firstChild) this.container.removeChild(this.container.firstChild);
		this.rooms.forEach(content=>{
			let item = new ListItem({container: this.container})
			item.render(content)
		})
		let buttons = document.getElementsByClassName('list-group-item-action')
		buttons.forEach(item =>{
			item.addEventListener('click', ()=>{
				this.roomSelected(item)}, true)
		})
	}
}
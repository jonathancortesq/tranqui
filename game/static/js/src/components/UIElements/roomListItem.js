


export default class ListItem{
	constructor(props){
		this.container = props.container
	}

	render(data){
		let html = `<a href="javascript:void(0)" id="room_${data.pk}" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" data-id="${data.pk}">
					    ${data.fields.name}
					    <span class="badge badge-primary badge-pill">${data.fields.players.length}</span>
					  </a>`
		this.container.innerHTML += html
		return true
	}
}
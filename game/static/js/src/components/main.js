import "babel-polyfill";
import * as Cookies from 'js-cookie'
import Axios from 'axios';
import Component from '../modules/baseComponent.js'
import store from '../store/index.js';
import roomList from './UIElements/roomList.js'
import playGround from './UIElements/playGround.js'


export default class Main extends Component {
    constructor(props){
        super({
          store,
          element: props.container
        });
        this.container = props.container
        this.params = new URLSearchParams(document.location.search.substring(1));
        this.rooms = new roomList()
        this.roomData = null
        let addRoomButton = document.getElementById('addRoom')
        addRoomButton.addEventListener("click", ()=>{this.showModal()}, true)
        this.rooms.getRooms()
        this.player = store.state.Player
        this.HTTP = Axios.create({
          baseURL: '/api/',
          headers:{
              'X-CSRFToken':Cookies.get('csrftoken')
          }
        })
    }

    showModal(){
        $('#createRoom').modal()
        let modal = document.getElementById('createRoom')
        let submitButton = modal.getElementsByClassName('btn-primary')
        let form = document.forms[0];
        let roomName = form['name']
        let matches = form['matches']
        submitButton[0].addEventListener('click',()=>{
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            }else{
                let data = {
                    'player': this.player.id,
                    'rules': {},
                    'name': roomName.value,
                    'matches_limit': parseInt(matches.value)
                }
                this.addRoom(data)
                form.reset()
            }
            form.classList.add('was-validated');
        }, false)
    }

    addRoom(data){
        this.HTTP.post('rooms/', data).then(response=>{
            $('#createRoom').modal('hide')
            this.rooms.getRooms()
        }).catch(error=>{
            console.log(error)
        })
    }

    render(){
        if(store.state.roomSelected && this.roomData != store.state.roomSelected){
            this.roomData = store.state.roomSelected
            const playContainer = document.getElementById('playground')
            const playZone = new playGround(playContainer)
            playZone.render(this.roomData)
        }
    }
}
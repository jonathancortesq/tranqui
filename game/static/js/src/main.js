import Main from './components/main.js'
import store from './store/index.js'; 

class GameApp {
    constructor(props){
        this.container = props.container.startsWith('#') ? document.getElementById(props.container.replace('#', '')) : document.querySelector(props.container)
        store.dispatch('Player', props.player )
        this.render()

    }

    render(){
        const main = new Main({container: this.container})
        main.render()
    }
}

/* Prototypes */

if (!Object.prototype.forEach) {
	Object.defineProperty(Object.prototype, 'forEach', {
		value: function (callback, thisArg) {
			if (this == null) {
				throw new TypeError('Not an object');
			}
			thisArg = thisArg || window;
			for (var key in this) {
				if (this.hasOwnProperty(key)) {
					callback.call(thisArg, this[key], key, this);
				}
			}
		}
	});
}

Element.prototype.appendBefore = function (element) {
  element.parentNode.insertBefore(this, element);
},false;

Element.prototype.appendAfter = function (element) {
  element.parentNode.insertBefore(this, element.nextSibling);
},false;


window.GameApp = GameApp
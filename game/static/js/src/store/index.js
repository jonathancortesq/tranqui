import actions from './modules/actions.js';
import mutations from './modules/mutations.js';
import state from './modules/state.js';
import Store from './modules/store.js';

export default new Store({
    actions,
    mutations,
    state
});
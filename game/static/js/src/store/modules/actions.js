export default {
    Player(context, payload) {
        context.commit('Player', payload);
    },
    roomSelected(context, payload) {
        context.commit('roomSelected', payload);
    },
    roomChange(context, payload){
        context.commit('roomChange', payload)
    },
    roomScoreChange(context, payload){
        context.commit('roomScoreChange', payload)
    },
    working(context, payload){
        context.commit('working', payload)
    },
    match(context, payload){
        context.commit('match', payload)
    }

};
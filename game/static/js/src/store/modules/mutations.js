export default {
    Player(state, payload) {
        state.Player = payload;
        return state;
    },
    roomSelected(state, payload) {
        state.roomSelected = payload
        return state;
    },
    roomChange(state, payload) {
        state.roomChange = payload
        return state;
    },
    roomScoreChange(state, payload){
        state.roomScoreChange = payload
        return state
    },
    working(state, payload){
        state.working = payload
        return state
    },
    match(state, payload){
        state.match = payload
        return state
    }

};
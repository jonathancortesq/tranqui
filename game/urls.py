# coding: utf-8
from django.conf.urls import include, url, handler403, handler404
from django.conf.urls.static import static
from django.conf import settings

from .views import *
# (?P<pk>[\w-]+)
urlpatterns = [
	url(r'^$', HomeView.as_view(), name='home'),
	url(r'^exit/', LogOutView.as_view(), name='exit'),
	url(r'^rooms/', RoomsView.as_view(), name='rooms'),
	url(r'^api/rooms/', APIRoom.as_view(), name='api_rooms'),
	url(r'^api/players/', APIPlayersList.as_view(), name='api_players'),
	url(r'^api/match/$', APIPMatch.as_view(), name='api_match'),
	url(r'^api/match/(?P<id>\d+)/$', APIPMatch.as_view(), name='api_match_game'),
]
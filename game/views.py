# coding: utf-8
import json
import random
from django.core import serializers
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.views.generic import (CreateView, TemplateView,
    RedirectView)
from django.views.generic.edit import FormMixin
from .forms import PlayerForm
from .models import Player, Room, Match

GAMECHOICES = {
    1:"rock",
    2:"spock",
    3:"paper",
    4:"lizard",
    5:"scissors",
}

MAPGAMMING = {
    "lizard": {4: "Was a tie", 5: "scissors", 1: "rock", 2: "lizard", 3: "lizard"},
    "paper": {3: "Was a tie", 4: "lizard", 5: "scissors", 1: "paper", 2: "paper"},
    "rock": {1: "Was a tie", 2: "spock", 3: "paper", 4: "rock", 5: "rock"},
    "scissors": {5: "Was a tie", 1: "rock", 2: "spock", 3: "scissors", 4: "scissors"},
    "spock": {2: "Was a tie", 3: "paper", 4: "lizard", 5: "spock", 1: "spock"},
}


class HomeView(CreateView):
    template_name = "home.html"
    form_class = PlayerForm
    success_url = '/rooms/'
    model = Player
    http_method_names = ['get', 'post']

    def form_valid(self, form):
        """
        Fake login
        """
        self.object = form.save()
        self.request.session['login'] = True
        self.request.session['player'] = {
            'id': self.object.id,
            'name': self.object.name
        }
        return super(HomeView, self).form_valid(form)


class LogOutView(RedirectView):
    url = '/'

    def get(self, request, *args, **kwargs):
        del request.session['login']
        del request.session['player']
        return super(LogOutView, self).get(request, *args, **kwargs)


class RoomsView(TemplateView):
    template_name = "room_game.html"

    def get(self, request, *args, **kwargs):
        if not request.session.get('login'):
            return HttpResponseRedirect('/')
        return super(RoomsView, self).get(request, *args, **kwargs)


class APIRoom(TemplateView):
    model = Room
    http_method_names = ['get', 'post', 'patch']

    def post(self, request, *args, **kwargs):
        post = json.loads(request.body)
        player = Player.objects.get(id=post.get('player'))
        rules = {
            'matches_limit': post.get('matches_limit')
        }

        room = self.model.objects.create(
            rules=rules,
            name=post.get('name')
        )
        room.players.add(player)
        data = serializers.serialize('json', [room])
        context = {
            'data': data,
            'status_code':200,
            'status': 'OK'
        }
        return JsonResponse(context, status=200)


    def get(self, request, *args, **kwargs):
        data = serializers.serialize('json', self.model.objects.all())
        context = {
            'data': json.loads(data),
            'status_code': 200,
            'status': 'OK'
        }
        return JsonResponse(context, status=200)

class APIPlayersList(TemplateView):
    model = Player

    def get(self, request, *args, **kwargs):
        queryset = self.model.objects.all()
        filters = request.GET.get('filters', None)
        if filters:
            filters = filters.split(',')
            queryset = queryset.filter(id__in=filters)
        data = serializers.serialize('json', queryset)
        context = {
            'data': json.loads(data),
        }
        return JsonResponse(context, status=200)

class APIPMatch(TemplateView):
    model = Match
    http_method_names = ['get', 'post', 'patch']
    Object = None

    def post(self, request, *args, **kwargs):
        post = json.loads(request.body)
        try:
            room = Room.objects.get(id=post.get('room_id'))
        except Room.DoesNotExist:
            return JsonResponse({'reason': 'Room does not exist'}, status=400)

        if room.rules.get('matches_limit', 0) > 0:
            if self.model.objects.filter(**post).exists():
                post.update({
                    'score': {},
                    'players_ready': {}
                })
                self.Object = self.model.objects.filter(**post).first()
                if not self.Object:
                    self.Object = self.model.objects.create(**post)  
            else:
                self.Object = self.model.objects.create(**post) 

            room.rules['matches_limit'] =-1
            room.save() 

            data = model_to_dict(self.Object)
            context = {
                'data': data,
                'status_code': 200,
                'status': 'OK'
            }
            return JsonResponse(context, status=200)
        else:
            return JsonResponse({'reason': 'Limit of matches was reached'}, status=400)        

    def patch(self, request, *args, **kwargs):
        patch = json.loads(request.body)
        try:
            self.Object = Match.objects.get(id=kwargs['id'])
        except (Match.DoesNotExist, KeyError):
            return JsonResponse({}, status=404)

        self.Object.choices.update(patch.get('choices'))
        self.Object.players_ready.update(patch.get('players_ready'))
        choices = [ value for _, value in self.Object.choices.items()]
        results = self.game_engine(choices[0])
        self.Object.save()
        player_score = self.player_score(results, patch.get('choices'))
        room_score = self.room_score(results, patch.get('choices'))
        self.Object = model_to_dict(self.Object)
        data = {
            'result': results,
            'data': self.Object,
            'player_score': player_score,
            'room_score': room_score
        }
       
        context = {
            'data': data,
            'status_code': 200,
            'status': 'OK'
        }
        return JsonResponse(context, status=200)

    def game_engine(self, choice):
        boot_choice = random.randint(1,5)
        MAPGAMMING[GAMECHOICES[boot_choice]][choice]
        result = {
            'boot_choice': boot_choice,
            'user_choise': choice,
            'winner': MAPGAMMING[GAMECHOICES[boot_choice]][choice]
        }

        return result

    def room_score(self, data, choices):
        room = self.Object.room
        for player_id, choice in choices.items():
            player = Player.objects.get(id=player_id)
            if GAMECHOICES[choices[player_id]] == data['winner']:
                try:
                    room.score[player.name] +=1
                except KeyError:
                    data = {}
                    data[player.name] = 1
                    room.score.update(data)
            elif data['winner'] == 'Was a tie':
                try:
                    room.score['tie'] +=1
                except KeyError:
                    room.score.update({'tie': 1})
            else:
                try:
                    room.score['boot'] +=1
                except KeyError:
                    room.score.update({'boot': 1})

        room.save()
        return room.score

    def player_score(self, data, choices):
        for player_id, choice in choices.items():
            player = Player.objects.get(id=player_id)
            if GAMECHOICES[choices[player_id]] == data['winner']:
                if player.score.get('winners'):
                    player.score['winners'] +=1
                else:
                    player.score.update({
                        'winners':1
                    })
            else:
                if player.score.get('loose'):
                    player.score['loose'] +=1
                else:
                    player.score.update({
                        'loose':1
                    })
            player.save()
        return player.score


